let fs = require('fs');
let childProcess = require('child_process');
let glob = require('glob');
let rimraf = require('rimraf');
let uglify = require('uglify-js');
let CleanCSS = require('clean-css');

function handleError(err) {
    // just log to the console
    console.error(err);
}

function getDistPath(filePath) {
    // get dist path from src path
    return filePath.replace(/src/, 'dist')
}

function replace(file) {
    // handle replacing file content for prod
    // remove all lines marked with remove:line
    // remove blocks of lines from remove:start (inclusive) to remove:end (inclusive)
    return file.toString().replace(/^.*(remove:line|remove:start(.|\n)*?remove:end).*$\n?/gm, '');
}

function ensurePath(path) {
    // ensure that the given path exists
    let parts = path.split('/'); // all path parts

    if (parts.length > 0) {
        // ensure we have parts
        if (parts[parts.length - 1].lastIndexOf('.') > 0 || parts[parts.length - 1].length === 0) {
            // remove the last part if it has a file extension or empty
            parts.pop();
        }

        if (parts.length > 0) {
            // ensure we still have parts
            let missingIndex = 0; // index of the first existing dir

            for (let i = parts.length; i > missingIndex; i--) {
                // starting from the end of the path
                // find the index of the first missing directory
                if (fs.existsSync(parts.slice(0, i).join('/'))) {
                    // this is the point where the directories do exist
                    missingIndex = i;
                }
            }

            for (let i = missingIndex + 1; i <= parts.length; i++) {
                // make the missing directories
                fs.mkdirSync(parts.slice(0, i).join('/'));
            }
        }
    }
}

function pathWrite(filePath, data, options) {
    // ensure the path to the file exists and write the file
    ensurePath(filePath);
    fs.writeFileSync(filePath, data, options);
}

// -------------------- clear dist --------------------
console.log('Clearing dist...');
rimraf.sync('dist/*');

// -------------------- copy files --------------------
console.log('Copying files...');

let copyPaths = glob.sync('src/{css/style.css,js/**/*.ts,templates/**/*.*,*.*}');

// replace content and copy files
copyPaths.forEach((filePath) => {
    let file = fs.readFileSync(filePath);

    pathWrite(getDistPath(filePath), replace(file));
});

// -------------------- compile typescript --------------------
console.log('Compiling typescript...');

// change config and compile then change back
let configFile = fs.readFileSync('tsconfig.json');

fs.writeFileSync('tsconfig.json', configFile.toString().replace(/src/g, 'dist'));

childProcess.exec('tsc', (err) => {
    if (err) {
        handleError(err);
    }

    fs.writeFile('tsconfig.json', configFile, (err) => {
        if (err) {
            handleError(err);
        }
    });

    // delete typescript files in dist
    glob('dist/js/**/*.ts', (err, filePaths) => {
        if (err) {
            handleError(err);
            return;
        }

        // delete files
        filePaths.forEach((filePath) => {
            fs.unlink(filePath, (err) => {
                if (err) {
                    handleError(err);
                }
            });
        });
    });

    // -------------------- minify javascript --------------------
    console.log('Minifying javascript...');

    let jsPaths = glob.sync('dist/**/*.js');

    // minify js files
    jsPaths.forEach((path) => {
        let file = fs.readFileSync(path).toString();
        let minJs = uglify.minify(file);

        if (minJs.error) {
            handleError(minJs.error);
            return;
        }

        fs.writeFileSync(path, minJs.code);
    });
});

// -------------------- minify css --------------------
console.log('Minifying css...');

let cssPath = 'dist/css/style.css';
let cssFile = fs.readFileSync(cssPath).toString();
let minCss = new CleanCSS().minify(cssFile);

// check for and handle errors
if (minCss.errors && minCss.errors.length > 0) {
    minCss.errors.forEach((err) => {
        handleError(err);
    })
}
else {
    fs.writeFileSync(cssPath, minCss.styles);
}
