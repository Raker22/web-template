#!/usr/bin/env bash
# variables used across multiple scripts
PROJECT="web-template" # project name
FIRST_DOMAIN="www.web-template.com" # this will be the name of a directory created by certbot
GET_CERTS=false # set to true if you want the get_certificates and renew_certificates to run
CERT_ARGS="-d $FIRST_DOMAIN -d web-template.com" # arguments used in get_certificates
HOST="" # the webapp host (either an ip or the domain name)
DIR=`cd $(dirname $"$0") && pwd` #the directory of the script being run

# functions
sedi () {
    # sed -i that works on mac and linux
    sed --version >/dev/null 2>&1 && sed -i -- "$@" || sed -i "" "$@"
}
