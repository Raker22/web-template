# Scripts

## build.js

A node script that builds the project from `src/` to `dist/`.

## deploy.sh

Deploys the production code to a remote server.

## dev.sh

Runs the app locally from `src/`.

## env.sh

The script that holds all of the variables used across multiple scripts.

## get_certificates.sh

Gets certificates issued from letsencrypt using certbot in a docker container. (for server use)

## get_self_signed.sh

Generates self signed certificates so the app can be run locally with the same settings as the production server.
(for local use)

## init_server.sh

Installs necessary tools to run the application on the host server.

## prod.sh

Runs the app locally from `dist\`.

## renew_certificates.sh

Renews certificates from letsencrypt using certbot in a docker container. (for server use)
