#!/usr/bin/env bash
# updates the link to renew_certificates in cron
. "$(dirname "$0")/env.sh"

if [ $GET_CERTS = true ]; then
    link_file="/etc/cron.monthly/renew_certificates"

    if [ -e "$link_file" ]; then
        sudo rm -f "$link_file"
    fi

    sudo ln -s "$DIR/renew_certificates.sh" "$link_file"
fi
