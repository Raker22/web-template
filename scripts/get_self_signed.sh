#!/usr/bin/env bash
# generate self signed certs so the app can be run with the same settings a production
. "$(dirname "$0")/env.sh"

key_dir="/docker/storage/$PROJECT/letsencrypt/etc/live/$FIRST_DOMAIN"
privkey="$key_dir/privkey.pem"
pubkey="$key_dir/fullchain.pem"

# only touch docker root folder as root
sudo mkdir /docker \
    /docker/storage \
    "/docker/storage/$PROJECT" \
    "/docker/storage/$PROJECT/letsencrypt" \
    "/docker/storage/$PROJECT/letsencrypt/etc" \
    "/docker/storage/$PROJECT/letsencrypt/etc/live" \
    "$key_dir"

sudo openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
    -subj "/C=US/ST=IN/L=Indianapolis/O=Dis/CN=$FIRST_DOMAIN" \
    -keyout "$privkey" -out "$pubkey"
