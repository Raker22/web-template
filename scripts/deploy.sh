#!/usr/bin/env bash
# deploy
. "$(dirname "$0")/env.sh"

# if the host variable from env.sh is empty then don't run
if [ -z "$HOST" ]; then
    echo "No host specified in env.sh"
else
    # prepare for prod
    npm run prod

    echo "Deploying app..."

    root_path="~/web"

    # delete the old website
    ssh -t "$HOST" "rm -rf $root_path; \
        mkdir $root_path"

    # copy files
    scp -r dist \
        scripts \
        package.json \
        package-lock.json \
        docker-compose.yml \
        default.conf \
        "$HOST:$root_path" \

    # install modules
    ssh -t "$HOST" "cd $root_path && npm install --production"

    # restart containers
    ssh -t "$HOST" "cd $root_path && docker-compose down; docker-compose up -d"
fi
