#!/usr/bin/env bash
# deploy the app locally in development mode
. "$(dirname "$0")/env.sh"

# update files for development use
sedi 's/dist/src/g' docker-compose.yml default.conf

# build
npm run compile

docker-compose down
docker-compose up -d

sedi 's/src/dist/g' docker-compose.yml default.conf
