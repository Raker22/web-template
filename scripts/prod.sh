#!/usr/bin/env bash
# deploy the app locally in production mode
. "$(dirname "$0")/env.sh"

# update files for production use
sedi 's/src/dist/g' docker-compose.yml default.conf

# build
npm run build

docker-compose down
docker-compose up -d
