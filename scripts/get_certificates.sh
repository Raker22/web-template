#!/usr/bin/env bash
# get certificates issued from letsencrypt
. "$(dirname "$0")/env.sh"

# only run if get certs is true
if [ $GET_CERTS = true ]; then
    docker pull palobo/certbot

    GetCert() {
        docker run -it \
            --rm \
            -v "/docker/storage/$PROJECT/letsencrypt/etc:/etc/letsencrypt" \
            -v "/docker/storage/$PROJECT/letsencrypt/lib:/var/lib/letsencrypt" \
            -v "/docker/storage/$PROJECT/letsencrypt/www:/usr/share/nginx/.well-known" \
            palobo/certbot -t certonly --webroot -w /usr/share/nginx \
            --keep-until-expiring \
            $@
    }

    echo "Getting certificates..."
    # nginx config can't have ssl or it won't run and therefore validation will fail
    sed -r -i 's/(.*)(ssl)/\1# \2/g' default.conf # replace ssl lines
    # restart
    docker-compose down
    docker-compose up -d

    # if the two domains are the same then only use one
    echo "$CERT_ARGS" | xargs GetCert

    echo "Restarting Website..."
    sed -r -i 's/(.*)# (ssl)/\1\2/g' default.conf # replace ssl config
    docker-compose down
    docker-compose up -d

    echo "Done"
fi
