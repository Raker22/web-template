#!/usr/bin/env bash
# renew certificates through letsencrypt
. "$(dirname "$0")/env.sh"

# only run if get certs is true
if [ $GET_CERTS = true ]; then
    docker pull palobo/certbot

    echo "Renewing Certs"

    docker run -it \
        --rm \
        -v "/docker/storage/$PROJECT/letsencrypt/etc:/etc/letsencrypt" \
        -v "/docker/storage/$PROJECT/letsencrypt/lib:/var/lib/letsencrypt" \
        -v "/docker/storage/$PROJECT/letsencrypt/www:/usr/share/nginx/.well-known" \
        palobo/certbot renew

    echo "Done"
fi