#!/usr/bin/env bash
# initialize the production server should be sourced not run
. "$(dirname "$0")/env.sh"

# install nvm
sudo apt-get install build-essential -y
sudo apt-get install libssl-dev -y

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash

# source nvm.sh so we can use nvm
. ~/.nvm/nvm.sh

nvm install 9
nvm use 9

# install docker (debian 9 Jessie or higher
sudo apt-get update

sudo apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common -y

curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -

sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
    $(lsb_release -cs) \
    stable"

sudo apt-get update

sudo apt-get install docker-ce -y

# get certificates and setup cronjob to renew certificates every month
eval "$DIR/get_certificates.sh"
eval "$DIR/update_renew_cron.sh"
