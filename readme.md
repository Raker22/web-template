# Web Template
A template for creating single page web applications.

## Features
* TypeScript
* Sass
* Knockout
* Bootstrap
* FontAwesome
* jQuery
* mocha/chai/sinon
* Custom Hash Router

## Getting Started
Run `npm run dev` to run the web app in development mode.

## NPM Scripts
* **compile** - install dependencies and compile typescript and scss in `src/`
* **test** - compile then run tests with mocha
* **build** - test then compile and copy files to `dist/`
* **dev** - compile then start the app locally in development mode
* **prod** - build then start the app locally in production mode
* **deploy** - build then deploy code to host server

## Starting  from Template
Find files that need to be updated with new project name using `grep "web-template" . -r`.
