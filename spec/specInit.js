// use requirejs instead of commonjs
require = require('requirejs');
let config = require('../../../src/requireConfig'); // load configuration from file

// reconfigure base url
config.baseUrl = 'src/js';
require.config(config);

// load the tests
require('../../spec/js/spec');
