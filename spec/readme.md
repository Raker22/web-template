# Testing
1. Create typescript file in `js/`
2. Include the file in a test set in `spec.ts`

The test result should output when running `npm test`.

Since tests are run in node, `specInit.js` configures requireJS
for node so the AMD modules in the project can be loaded.

See `spec/js/viewModels/` for examples.

## Notes
* Spies don't detect calls that are the result of being notified by an observable.
