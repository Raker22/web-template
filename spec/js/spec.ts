import {TestSet} from '../../spec/js/TestSet';

const testBaseUrl: string = '../../spec/js/'; // the relative path to the test directory from tsconfig baseUrl
const testExtension: string = '.spec'; // the extension that test files have (before .ts)

// add a TestSet for each set (directory) of tests that we're running
const testSets: TestSet[] = [
    new TestSet([
        'TestSet'
    ]),
    new TestSet([
        'AbstractTemplateView',
        'AbstractView'
    ], 'viewModels')
];

// run all of the test sets
testSets.forEach((testSet: TestSet) => {
    // run all of the tests in the test set
    testSet.getTests().forEach((test: string) => {
        require(`${testBaseUrl}${test}${testExtension}`);
    });
});
