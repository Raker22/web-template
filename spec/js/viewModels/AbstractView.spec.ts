import {AbstractView} from 'viewModels/AbstractView';
import * as chai from 'chai';

chai.should();

class TestView extends AbstractView {

}

describe('AbstractView', () => {
    describe('constructor', () => {
        let testString: string = 'spec string';
        let testView: AbstractView  = new TestView(testString);

        it('initializes page from arguments', () => {
            testView.page().should.equal(testString);
        });

        it('initializes url from arguments', () => {
            testView.url.should.be.a('function'); // technically a KnockoutComputed
        });
    });
});
