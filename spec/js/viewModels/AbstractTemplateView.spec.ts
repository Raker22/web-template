import {AbstractTemplateView} from 'viewModels/AbstractTemplateView';
import {TemplateView} from 'viewModels/TemplateView';
import {SinonSpy} from 'sinon';
import * as chai from 'chai';
import * as sinon from 'sinon';
import * as sinon_chai from 'sinon-chai';

chai.use(sinon_chai);
chai.should();

class TestView extends AbstractTemplateView {

}

describe('AbstractView', () => {
    let testView: TemplateView  = new TestView('');
    let oldView: TemplateView = new TestView('old view');
    let newView: TemplateView = new TestView('new view');

    describe('setCurrentView', () => {
        beforeEach(() => {
            testView.currentView(oldView);
        });

        it('does nothing when the new view is the same as the current view', () => {
            let currentViewSpy: SinonSpy = sinon.spy(testView, 'currentView');
            testView.setCurrentView(oldView);
            currentViewSpy.should.not.have.been.calledWith(oldView);
        });

        it('sets the current view', () => {
            testView.setCurrentView(newView);
            testView.currentView().should.equal(newView);
        });
    });

    describe('onCurrentViewChanged', () => {
        beforeEach(() => {
            testView.currentView(oldView);
        });

        it('calls the new view\'s onBound method', () => {
            let onBoundSpy: SinonSpy = sinon.spy(newView, 'onBound');
            testView.currentView(newView);
            onBoundSpy.should.have.been.calledOnce;
        });
    });
});
