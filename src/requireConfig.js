'use strict';
define(function() {
    var vendorPath = '../../node_modules/'; // remove:line
    /* remove:line
    var vendorPath = '../node_modules/';
    remove:line */

    var paths = {
        requirejs: 'requirejs/require',
        // remove:start
        bootstrap: 'bootstrap/dist/js/bootstrap.bundle',
        jquery: 'jquery/dist/jquery',
        knockout: 'knockout/build/output/knockout-latest.debug',
        'knockout.mapping': 'knockout-mapping/dist/knockout.mapping'
        // remove:end
        /* remove:line
        bootstrap: 'bootstrap/dist/js/bootstrap.bundle.min',
        jquery: 'jquery/dist/jquery.min',
        knockout: 'knockout/build/output/knockout-latest',
        'knockout.mapping': 'knockout-mapping/dist/knockout.mapping.min'
        // remove:line */
    };

    for (var key in paths) {
        paths[key] = vendorPath + paths[key];
    }

    return {
        baseUrl: 'js',
        paths: paths,
        packages: [],
        shim: {}
    };
});
