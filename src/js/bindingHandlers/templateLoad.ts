import * as ko from 'knockout';
import {View} from 'viewModels/View';

/**
 * A binding handler that dynamically loads content into
 * the element it is bound to.
 *
 * The binding handler expects the valueAccessor to be an
 * Object with a url parameter specifying the path to the
 * content that will be loaded into the element.
 * The url must be a string when unwrapped.
 * If the valueAccessor has an onLoad method it will be
 * called after the content has been loaded.
 * No parameters will be passed to the onLoad method.
 *
 * The valueAccessor will also be bound to the contents so
 * it can be a viewModel with a url property.
 *
 * @example
 * // load the contents of home.html into a div
 * <div data-bind="templateLoad = { url: 'home.html' }"></div>
 */

ko.bindingHandlers['templateLoad'] = {
    init: (): void => {},
    update: (element: Element, valueAccessor: KnockoutObservable<any>): void => {
        let viewModel: View = ko.unwrap(valueAccessor());

        // Clear content so observables don't throw an error when the view model changes
        element.innerHTML = '';

        if (viewModel) {
            // Don't load content if there isn't data
            let url: string = ko.unwrap(viewModel.url);

            if (url) {
                // Don't load content if there isn't a url
                let xhttp: XMLHttpRequest = new XMLHttpRequest();

                xhttp.onreadystatechange = (): void => {
                    if (xhttp.readyState === XMLHttpRequest.DONE) {
                        if (xhttp.status >= 200 && xhttp.status < 300) {
                            element.innerHTML = xhttp.responseText;
                            ko.applyBindingsToDescendants(valueAccessor, element);

                            if (viewModel.onLoad != null && typeof viewModel.onLoad === 'function') {
                                viewModel.onLoad();
                            }
                        }
                        else {
                            console.error(new Error(`Template Load (${url}) - ${xhttp.status} ${xhttp.statusText}`));
                        }
                    }
                };

                xhttp.open('GET', url, true);
                xhttp.send();
            }
        }
    }
};
