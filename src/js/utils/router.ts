import {Router} from 'Router';
import {baseView} from 'viewModels/baseView';
import {defaultView} from 'viewModels/defaultView';
import {homeView} from 'viewModels/homeView';
import {scriptsView} from 'viewModels/scriptsView';
import {styleView} from 'viewModels/styleView';
import {testsView} from 'viewModels/testsView';

export let router: Router = new Router();

router.add('', (): boolean => {
    // this route is always executed
    $('#base_nav_collapse').collapse('hide');
    $('#base_navbar .active').removeClass('active');

    return false; // if all matching routes return false the default method will still be called
});

router.add(/^$/, () => {
    // home route
    $('#base_nav_home').addClass('active');
    baseView.setCurrentView(homeView);
});

router.add(/^scripts$/, () => {
    // home route
    $('#base_nav_scripts').addClass('active');
    baseView.setCurrentView(scriptsView);
});

router.add(/^style$/, () => {
    // home route
    $('#base_nav_style').addClass('active');
    baseView.setCurrentView(styleView);
});

router.add(/^tests$/, () => {
    // home route
    $('#base_nav_tests').addClass('active');
    baseView.setCurrentView(testsView);
});

router.default = (): void => {
    baseView.setCurrentView(defaultView);
};
