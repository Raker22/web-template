export class Route {
    path: RegExp; // the regex to match against the hash
    callback: (context: string[]) => any;

    constructor(path: RegExp, callback: (params: string[]) => any) {
        this.path = path;
        this.callback = callback;
    }
}

export class Router {
    // all of the added routes
    routes: Route[] = [];
    default: () => any = () => {}; // called if no route matches

    static REPLACE_REGEX: string = '[^/]*';

    static stringToRegex(path: string): RegExp {
        // convert a string path to a regex path
        return new RegExp(`^${ // prepend ^
            path.replace(/\*/g, Router.REPLACE_REGEX) // replace * with 0 or more non '/' regex
                .replace(/{}/g, `(${Router.REPLACE_REGEX})`) // replace {} with a capture group
        }`);
    }

    constructor() {
        window.addEventListener('hashchange', () => this.run());
        window.addEventListener('load', () => this.run());
    }

    add(path: string | RegExp, callback: (params: string[]) => any): void {
        if (typeof path === 'string') {
            // convert the string to a regex
            path = Router.stringToRegex(path);
        }

        this.routes.push(new Route(path, callback));
    }

    run(): void {
        // execute all matching routes or the default if none match
        let hash: string = window.location.hash.substr(1); // remove first char (#)
        let hit: boolean = false; // if any route was executed

        this.routes.forEach((route: Route) => {
            let params: RegExpMatchArray = hash.match(route.path);

            if (params != null) {
                // the route matches
                params.shift(); // remove the firs result (the entire matched string)
                let result: any = route.callback(params); // pass the params into the callback

                hit = hit || result === undefined || result; // if the callback comes back false we don't have a hit
            }
        });

        if (!hit) {
            this.default();
        }
    }
}
