import {AbstractView} from 'viewModels/AbstractView';

export class StyleView extends AbstractView {
    constructor() {
        super('style.html');
    }
}

export let styleView: StyleView = new StyleView();
