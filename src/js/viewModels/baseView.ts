import {AbstractTemplateView} from 'viewModels/AbstractTemplateView';
import {router} from 'utils/router';

export class BaseView extends AbstractTemplateView {
    constructor() {
        super('base.html');
    }

    onLoad(): void {
        $('body')
            .css('padding-top', $('#base_navbar').outerHeight())
            .css('padding-bottom', $('#base_footer').outerHeight());

        router.run();
    }
}

export let baseView: BaseView = new BaseView();
