import * as ko from 'knockout';

import {AbstractView} from 'viewModels/AbstractView';
import {TemplateView} from 'viewModels/TemplateView';
import {View} from 'viewModels/View';

export abstract class AbstractTemplateView extends AbstractView implements TemplateView {
    currentView: KnockoutObservable<View> = ko.observable(null);

    constructor(page: string) {
        super(page);

        this.currentView.subscribe(this.onCurrentViewChanged);
    }

    setCurrentView(newView: View): void {
        // if newView and currentView are the same don't rebind/reload the view
        if (this.currentView() !== newView) {
            this.currentView(newView);
        }
    }

    onCurrentViewChanged(newView: View): void {
        // if the view does change then call its onBound function
        if (newView != null) {
            newView.onBound();
        }
    }
}
