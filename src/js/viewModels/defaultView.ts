import {AbstractTemplateView} from 'viewModels/AbstractTemplateView';

export class DefaultView extends AbstractTemplateView {
    constructor() {
        super('default.html');
    }
}

export let defaultView: DefaultView = new DefaultView();
