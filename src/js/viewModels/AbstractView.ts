import * as ko from 'knockout';

import {View} from 'viewModels/View';

export abstract class AbstractView implements View {
    static TEMPLATE_PATH: string = 'templates'; // the root path of the template pages that views load

    url: KnockoutComputed<string>;
    page: KnockoutObservable<string> = ko.observable(null); // the specific page to load from TEMPLATE_PATH

    constructor(page: string) {
        // construct a view with url templates/page
        this.page(page);

        this.url = ko.pureComputed((): string => {
            return `${AbstractView.TEMPLATE_PATH}/${this.page()}`;
        });
    }

    onBound(): void {
        // do nothing by default
    }

    onLoad(): void {
        // do nothing by default
    }
}
