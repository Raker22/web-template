import {AbstractTemplateView} from 'viewModels/AbstractTemplateView';
import {baseView} from 'viewModels/baseView';

export class AppView extends AbstractTemplateView {
    constructor() {
        super(''); // this view is bound to the page in app.ts so it doesn't need a page
        this.currentView(baseView);
    }
}

export let appView: AppView = new AppView();
