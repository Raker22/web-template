import {AbstractView} from 'viewModels/AbstractView';

export class ScriptsView extends AbstractView {
    constructor() {
        super('scripts.html');
    }
}

export let scriptsView: ScriptsView = new ScriptsView();
