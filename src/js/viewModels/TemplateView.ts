import {View} from 'viewModels/View';

export interface TemplateView extends View {
    currentView: KnockoutObservable<View>; // the view bound and loaded into the template view
    setCurrentView(newView: View): void; // sets the current template view
    onCurrentViewChanged(newView: View): void; // called when currentView is changed
}
