import {AbstractView} from 'viewModels/AbstractView';

export class HomeView extends AbstractView {
    constructor() {
        super('home.html');
    }
}

export let homeView: HomeView = new HomeView();
