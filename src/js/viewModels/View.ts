export interface View {
    url: KnockoutComputed<string>; // the url of the page to load
    onBound(): void; // called when the view starts to be actively used on the page
    onLoad(): void; // called when the view's template page has been loaded into the DOM
}
