import {AbstractView} from 'viewModels/AbstractView';

export class TestsView extends AbstractView {
    constructor() {
        super('tests.html');
    }
}

export let testsView: TestsView = new TestsView();
