import * as ko from 'knockout';
import 'bootstrap';
import 'jquery';
import 'knockout.mapping';

import 'bindingHandlers';
import {appView} from 'viewModels/appView';

ko.applyBindings(appView);

// remove:start
import {router} from 'utils/router';

window['router'] = router;
window['ko'] = ko;
window['appView'] = appView;
// remove:end