# Web Template Styles
Each view has its own file for styling. (e.x. `_home.scss`)

`style.scss` includes each view's file as well as external Sass files from
services like Bootstrap and FontAwesome.
